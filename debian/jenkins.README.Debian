Jenkins for Debian
------------------

This package differs in the following ways from the upstream distribution of
Jenkins:
 
 * A subset of the default plugins are bundled, most notably the following
   plugins aren't installed by default:
    - SSH Slaves Plugin
    - Maven Integration Plugin
    - Subversion Plugin
   Note that Jenkins for Debian retains binary compatibility with upstream
   plugins so these can be installed through the 'Manage Plugins' option 
   with Jenkins.

 * Some plugins assume the default plugins are always installed and do not
   declare an explicit dependency. If you encounter a ClassNotFoundException
   you may have to find the relevant dependencies in the plugins management
   panel and install them.

 * Native OS integration features that are disabled:
    - Solaris: support for libzfs and libembeddedsu4j.
    - Windows: native control of remote windows slaves, native integration
      with Windows process management, windows service deployment of
      Jenkins.
   Note that it is possible to use Windows slaves; however they will need
   to be launched using JNLP from the slave itself.

 * Miscellanous dependency changes that Developers should be aware of:
    - jna-posix -> jnr-posix (inline with upstream project renaming)

 * Branding: if available on the client, the Web UI will use the Ubuntu 
   font.

 * Packages + wrapper scripts are provided for:
    - Monitoring of arbitrary jobs: jenkins-external-job-monitor
	  + see man jenkins-monitor-job for more details.
    - Jenkins CLI: jenkins-cli
      + see man jenkins-cli for more details.


Master/Slave Security Considerations
------------------------------------

Jenkins master and slaves behave as if they altogether form a single
distributed process. This means a slave can ask a master to do just about
anything within the confinement of the operating system, such as accessing
files on the master or trigger other jobs on Jenkins. Therefore adding
untrusted slaves to the cluster is not recommended.
